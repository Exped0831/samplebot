from slackbot.bot import respond_to     # @botname: で反応するデコーダ
from slackbot.bot import listen_to      # チャネル内発言で反応するデコーダ
from slackbot.bot import default_reply  # 該当する応答がない場合に反応するデコーダ

@respond_to('メンション')
def mention_func(message):
    message.reply('私にメンションと言ってどうするのだ') # メンション

@listen_to('リッスン')
def listen_func(message):
    message.send('誰かがリッスンと投稿したようだ')      # ただの投稿
    message.reply('君だね？')

@listen_to("しんど(.*)")
@listen_to("疲れた(.*)")
@listen_to("つかれた(.*)")
def cheeryouup(message, something):
  message.send("http://sportsnews.xsrv.jp/wp-content/uploads/2014/10/6c2a5222b198dc1f222bf3d4d0301adb.jpg")

@respond_to('今日の天気')
def weather_today(message):
  import urllib3
  import json
  import requests
  from plugins.scripts.util import concatstr

  url = 'http://weather.livedoor.com/forecast/webservice/json/v1?city=400010'
  tenki_data = requests.get(url).json()

  strarray = []
  strarray.append(tenki_data['title'])
  strarray.append(tenki_data['forecasts'][0]['date'])
  strarray.append("今日の天気は" + tenki_data['forecasts'][0]['telop'] + "や")
  if tenki_data['forecasts'][0]['temperature']['max'] is not None:
    strarray.append("今日の最高気温は" + tenki_data['forecasts'][0]['temperature']['max']['celsius'] + "度や")
  else:
    strarray.append("もうこんな時間やし今日の最高気温は忘れたなぁ、すまんね！")

  message.send(concatstr(strarray))

@respond_to('明日の天気')
def weather_tomorrow(message):
  import urllib3
  import json
  import requests
  from plugins.scripts.util import concatstr

  url = 'http://weather.livedoor.com/forecast/webservice/json/v1?city=400010'
  tenki_data = requests.get(url).json()

  strarray = []
  strarray.append(tenki_data['title'])
  strarray.append(tenki_data['forecasts'][1]['date'])
  strarray.append("明日の天気は" + tenki_data['forecasts'][1]['telop'] + "や")
  strarray.append("明日の最高気温は" + tenki_data['forecasts'][1]['temperature']['max']['celsius'] + "度や")
  message.send(concatstr(strarray))

@respond_to("よ")
def say_yo(message):
  message.send("よう! 調子はどうや？")

#EC2からビーイングにアクセスできんかった～
@respond_to("日報_(.*)")
def nippo(message,something):
  import time
  from selenium import webdriver
  from selenium.webdriver.chrome.options import Options
  from selenium.webdriver.common.keys import Keys
  from selenium.webdriver.common.action_chains import ActionChains

  chrome_options = Options()
  chrome_options.add_argument('--headless')
  chrome_options.add_argument('--no-sandbox')
  chrome_options.add_argument('--disable-dev-shm-usage')
  driver = webdriver.Chrome(executable_path='C:/Users/takayama/AppData/Local/Programs/Python/Python37/Lib/site-packages/chromedriver_binary/chromedriver',chrome_options=chrome_options)
  driver.get("http://dev-ss/Nipo/control/ComCtl.php?actioncode=1001")
  time.sleep(2)

  driver.find_element_by_name('inuserid').send_keys("761")
  driver.find_element_by_name('inpassword').send_keys("761")
  driver.find_element_by_name('button').click()
  time.sleep(2)

  driver.find_element_by_name('Job1').send_keys(something)
  driver.find_element_by_css_selector('table:nth-child(1) > tbody > tr > td:nth-child(1) > input').click()
  time.sleep(5)

  driver.find_element_by_name('MenuMainLogout').click()
  time.sleep(3)

  driver.quit()

  message.send("http://dev-ss/Nipo/control/ComCtl.php?actioncode=2001")

@respond_to("検索_(.*)")
def search(message, something):
  import time
  from selenium import webdriver
  from selenium.webdriver.chrome.options import Options
  from selenium.webdriver.common.keys import Keys
  from selenium.webdriver.common.action_chains import ActionChains

  chrome_options = Options()
  chrome_options.add_argument('--headless')
  chrome_options.add_argument('--no-sandbox')
  chrome_options.add_argument('--disable-dev-shm-usage')
  driver = webdriver.Chrome(executable_path='C:/Users/takayama/AppData/Local/Programs/Python/Python37/Lib/site-packages/chromedriver_binary/chromedriver', chrome_options=chrome_options)
  driver.get("https://www.google.co.jp/")
  #executable_path='/usr/lib/python3.6/site-packages/chromedriver_binary/chromedriver'
  #C:/Users/takayama/AppData/Local/Programs/Python/Python37/Lib/site-packages/chromedriver_binary/chromedriver

  time.sleep(1)

  text = something

  driver.find_element_by_name('q').send_keys(text)
  actions = ActionChains(driver)
  actions.send_keys(Keys.ENTER).perform()

  time.sleep(2)

  seach_res = [link.text for link in driver.find_elements_by_css_selector('div.r > a')]

  if seach_res is None :
    message.send("何の結果も得られなかったわ、すまんな！")
    driver.quit()

  else:
    message.send(seach_res[0])
    driver.quit()

@respond_to("名言")
def todays_quote(message):
  import requests
  from bs4 import BeautifulSoup

  r = requests.get('http://www.meigensyu.com/quotations/view/random')
  data = BeautifulSoup(r.text, 'html.parser')
  elems = data.find_all("div", class_="text")
  message.send(elems[0].getText())

@respond_to("ニュース")
def todays_quote(message):
  import requests
  from bs4 import BeautifulSoup

  r = requests.get('https://news.yahoo.co.jp')
  data = BeautifulSoup(r.text, 'html.parser')
  elems = data.find_all("li", class_="topicsListItem")

  news = ""
  for i in range(4):
    news += elems[i].getText() + "\n"

  news += "気になるニュースは自分で確認してな！\n"
  news += "https://news.yahoo.co.jp"

  message.send(news)

@respond_to("株価教えて")
def show_stack(message):
  import requests
  from bs4 import BeautifulSoup

  r = requests.get('https://www.nikkei.com/nkd/company/?scode=4734')
  data = BeautifulSoup(r.text, 'html.parser')
  elems = data.find_all("span", class_="m-stockInfo_detail_value")

  msg = "始値は" + elems[0].getText() + "\n"
  msg += "高値は" + elems[1].getText() + "\n"
  msg += "安値は" + elems[2].getText() + "やで！\n"

  message.send(msg)

@respond_to("設備予約/今日")
def trial_method(message):
  txt = "7/30 14時現在\n"
  txt += "応接室:打ち合わせ:開発 14:30-15:30\n"
  txt += "プロジェクタ:特にありません。\n"
  txt += "会議室:打ち合わせ:PXP 15:30-18:00\n"

  message.send(txt)

@respond_to("今日の占い/しし座")
def trial_seiza(message):
  txt = "2位！運気は安定♪ステキな人からアプローチされて、ときめきを感じそうです。話してみると意気投合できるので、積極的に。\n"
  txt += "金運:★★★★★\n"
  txt += "仕事運:★★★★★\n"
  txt += "恋愛:★★★★★\n"
  txt += "ラッキーアイテム:新しい下着\n"
  txt += "ラッキーカラー:ピンク\n"

  message.send(txt)

"""
@respond_to("不審者情報")
def show_fushinsya(message):
  import requests
  from bs4 import BeautifulSoup

  r = requests.get('http://www.anzen-fukuoka.jp/an2net/fmail_list.php')
  data = BeautifulSoup(r.text, 'html.parser')
  title = data.find("h4", class_="mail_back_content_title")
  date = data.find("div", class_="mail_back_content_box_text")

  meg = "福岡の最新の不審者情報を教えるよ！\n"
  message.send(data)
"""
import pya3rt
